export const CONSTANTS = {
  home: {
    homePageTitle: 'Accueil'
  },
  errorPage: {
    defaultTitle: 'Ooops..',
    notFound: 'Page Introuvable',
    errorDescription: 'Oopps !! The page you are looking for does not exist.',
    wrongErrorTitle: 'Something went wrong',
    wrongErrorDescription: `Looks like something went wrong.<br> We're working on it`
  },
  auth: {
    loginSuccessMsg: 'Vous etes à present connecté',
    logoutSuccessMsg: 'Vous etes à present deconnecté',
  }
};
