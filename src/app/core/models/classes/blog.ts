import { BlogData } from "../interfaces/blog-data";

export class Blog {
    userId!: number;
    id!: number;
    title!: string;
    body!: string;

    constructor(data: BlogData) {
        Object.assign(this, data);
    }
}