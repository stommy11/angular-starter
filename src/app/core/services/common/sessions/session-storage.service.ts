import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {
  
  /**
   * Session Storage
   */
  constructor(@Inject(SESSION_STORAGE) private storage: StorageService) {}

   /**
    * Set session
    */
  public setSession(key: any, content?: any): Observable<any> {
    return of(this.storage.set(key, content));
  }

   /**
    * Get session
    */
  public getSession(key: any): Observable<any> {
    if (this.hasSession(key)) {
      return of(this.storage.get(key));
    }

    return of('session not exist');
  }

   /**
    * has session
    */
  public hasSession(key: any): Observable<boolean> {
    return of(this.storage.has(key));
  }

   /**
    * Clear session
    */
  public clearSession(): Observable<boolean> {
    this.storage.clear();

    return of(true);
  }
}
