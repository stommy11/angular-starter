import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ApiService } from "@core/services/api/api.service";
import { Blog } from '@core/models/classes/blog';
import { BlogData } from '@core/models/interfaces/blog-data';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private api: ApiService<Blog>) { }

  /**
   * Gets blog datas
   * @returns Blog[]
   */
  getBlogs(): Observable<Blog[]> {
    return this.api.get<Blog[]>(`assets/static/fake-blog-data.json`).pipe(
      map((response: any) => response.map((data: BlogData) => new Blog(data)),
      catchError((error: HttpErrorResponse) => observableThrowError(error)))
    )
  }
}
