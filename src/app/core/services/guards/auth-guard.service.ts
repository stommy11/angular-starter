import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { SessionStorageService } from '../common/sessions/session-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  /**
   * Creates an instance of auth guard.
   * @param router [Angular router service]
   * @param sessionStorage [Session service]
   */
  constructor(
    private router: Router,
    private session: SessionStorageService) {}

  /**
   * Determines whether activate can
   * @param next [Angular activated route snapshot]
   * @param state [Angular router snapshot]
   * @returns activate
   */
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.session.hasSession('access_token').pipe(
      map(res => {
        if (!res) {
          this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } });
        }
        return res;
      }),
      first()
    );
  }
}
