import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/services/guards/auth-guard.service';
import { BaseComponent } from './features/layout/base/base.component';

const AUTH = 'auth';
const MAIN = 'main';
const ERROR = 'error';
const FULLPATH = 'full';

const routes: Routes = [
  { path: AUTH, loadChildren: () => import('./features/pages/auth/auth.module').then(m => m.AuthModule) },
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: MAIN,
        loadChildren: () => import('./features/pages/main/main.module').then(m => m.MainModule)
      },
      { path: '', redirectTo: MAIN, pathMatch: FULLPATH },
    ]
  },
  {
    path: ERROR,
    loadChildren: () => import('./features/pages/error-page/error-page.module').then(m => m.ErrorPageModule)
  },
  { path: '**', redirectTo: ERROR, pathMatch: FULLPATH }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
