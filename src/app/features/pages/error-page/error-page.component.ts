import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HTTP_STATUS_CODE } from "@core/models/enums/http-status-code";
import { CONSTANTS } from '@core/constants/constants';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html'
})
export class ErrorPageComponent implements OnInit, OnDestroy {
  type!: string | null;
  title!: string;
  desc!: string;
  readonly CONSTANTS = CONSTANTS;
  private _destroy$ = new Subject<void>();

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.type = this.route.snapshot.paramMap.get('type');
    
    this.route.data.pipe(takeUntil(this._destroy$)).subscribe(param => {
      if(param.type) {
        this.type = param.type;
      }

      if(param.title) {
        this.title = param.title;
      }

      if(param.desc) {
        this.desc = param.desc
      }
    });

    switch(this.type) {
      case HTTP_STATUS_CODE.NOT_FOUND:
        if (!this.title) {
          this.title = CONSTANTS.errorPage.notFound
        }
        
        if (!this.desc) {
          this.desc = CONSTANTS.errorPage.errorDescription
        }
        break;
        
      default:
        if (!this.type) {
          this.type = CONSTANTS.errorPage.defaultTitle;
        }

        if (!this.title) {
          this.title = CONSTANTS.errorPage.wrongErrorTitle;
        }

        if (!this.desc) {
          this.desc = CONSTANTS.errorPage.wrongErrorDescription;
        }
    }
  }

	ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }
}
