import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CONSTANTS } from '@core/constants/constants';
import { HTTP_STATUS_CODE } from '@core/models/enums/http-status-code';
import { ErrorPageComponent } from './error-page.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorPageComponent,
    data: {
      type: HTTP_STATUS_CODE.NOT_FOUND,
      title: CONSTANTS.errorPage.notFound,
      desc: CONSTANTS.errorPage.errorDescription
    }
  },
  {
    path: ':type',
    component: ErrorPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorPageRoutingModule { }
