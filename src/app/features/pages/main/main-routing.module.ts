import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const HOME_ROUTE = 'home';

const routes: Routes = [
  {
    path: HOME_ROUTE,
    loadChildren: () => import('./home/home.module').then((mod) => mod.HomeModule),
    data: { title: 'App home' },
    pathMatch: 'full',
  },
  { path: '', redirectTo: HOME_ROUTE, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
