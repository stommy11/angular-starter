import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SessionStorageService } from '@core/services/common/sessions/session-storage.service';
import { CONSTANTS } from '@core/constants/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  returnUrl!: string;
  readonly CONSTANTS = CONSTANTS;
  private _destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private session: SessionStorageService) { }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';  
  }

  _onLogin() {
    this.session.setSession('access_token', { islogged: true });
    this.session.hasSession('access_token').pipe(takeUntil(this._destroy$)).subscribe(resp => {
      if (resp) {
        this.router.navigate([this.returnUrl]);
      }
    });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

}
