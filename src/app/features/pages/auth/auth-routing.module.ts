import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const LOGIN = 'login';
const REGISTER = 'register';

const routes: Routes = [
  {
    path: LOGIN,
    component: LoginComponent,
    data: { title: LOGIN }
  },
  {
    path: REGISTER,
    component: RegisterComponent,
    data: { title: REGISTER }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
