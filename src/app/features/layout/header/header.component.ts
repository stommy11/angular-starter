import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SessionStorageService } from '@core/services/common/sessions/session-storage.service';
import { CONSTANTS } from '@core/constants/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {

  readonly CONSTANTS = CONSTANTS;
  private _destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private sessionStorageService: SessionStorageService
  ) { }

  ngOnInit(): void {
  }

  _onLogout() {
    this.sessionStorageService.clearSession().pipe(takeUntil(this._destroy$)).subscribe(() => {
      this.router.navigate(['/auth/login']);
    });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

}
