import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';

const BASE_MODULES = [CommonModule];
const ENTRY_COMPONENTS: any[] = [];
const COMPONENTS = [LoaderComponent];
const PIPES: any[] = [];
const DIRECTIVES: any[] = [];

@NgModule({
  declarations: [...COMPONENTS, ...PIPES, ...DIRECTIVES],
  imports: [...BASE_MODULES],
  exports: [...BASE_MODULES, ...COMPONENTS, ...PIPES, ...DIRECTIVES],
  entryComponents: [...ENTRY_COMPONENTS]
})
export class SharedModule { }
